use inc::Module::Install;

# Define metadata
name           'FetchCVE';
abstract       'Connects to NVD web site and scrapes information about a single CVE';
author         'David Walker <dwalker@handshake.hk>';
version_from   'lib/FetchCVE.pm';
license        'copyright 2014 Handshake Networking Ltd';
perl_version   '5.014';

# Specific dependencies
# requires 'File::Spec'  => '0.80';
# requires 'Carp';
# requires 'Carp::Assert';
# requires 'List::MoreUtils';
# requires 'Readonly';
# requires 'English';
# requires 'Scalar::Util';
# requires 'File::Path';
# requires 'Cwd';
# requires 'File::HomeDir';
# requires 'Template';
# requires 'Perl6::Slurp';
# requires 'Time::ParseDate';
# requires 'feature' ;
# requires 'Config::General';
# requires 'Text::Wrap';
# requires 'File::ShareDir';

install_script 'bin/fetchcve';

WriteAll;

# use 5.008008;
# use ExtUtils::MakeMaker;
